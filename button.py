import RPi.GPIO as gpio
import time

gpio.setmode(gpio.BCM)
gpio.setup(17, gpio.IN, pull_up_down=gpio.PUD_UP)
gpio.setup(27, gpio.IN, pull_up_down=gpio.PUD_UP)

while True:
    input_value = gpio.input(17)
    input_value2 = gpio.input(27)
    if input_value == False:
        print("Pushed 1")
        while input_value == False:
            input_value = gpio.input(17)
            time.sleep(0.2)
    elif input_value2 == False:
        print("Pushed 2")
        while input_value2 == False:
            input_value2 = gpio.input(27)
            time.sleep(0.2)