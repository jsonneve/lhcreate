import pygame
import numpy as np
import playkinect
import sys
from freenect import sync_get_depth as get_depth
from freenect import sync_get_video as get_video
import Image
import sys
from PIL import ImageOps

import RPi.GPIO as gpio
import time

gpio.setmode(gpio.BCM)
gpio.setup(17, gpio.IN, pull_up_down=gpio.PUD_UP)
gpio.setup(27, gpio.IN, pull_up_down=gpio.PUD_UP)

def make_gamma():
    """
    Create a gamma table
    """
    num_pix = 2048 # there's 2048 different possible depth values
    npf = float(num_pix)
    _gamma = np.empty((num_pix, 3), dtype=np.uint16)
    for i in xrange(num_pix):
        v = i / npf
        v = pow(v, 3) * 6
        pval = int(v * 6 * 256)
        lb = pval & 0xff
        pval >>= 8
        if pval == 0:
            a = np.array([255, 255 - lb, 255 - lb], dtype=np.uint8)
        elif pval == 1:
            a = np.array([255, lb, 0], dtype=np.uint8)
        elif pval == 2:
            a = np.array([255 - lb, lb, 0], dtype=np.uint8)
        elif pval == 3:
            a = np.array([255 - lb, 255, 0], dtype=np.uint8)
        elif pval == 4:
            a = np.array([0, 255 - lb, 255], dtype=np.uint8)
        elif pval == 5:
            a = np.array([0, 0, 255 - lb], dtype=np.uint8)
        else:
            a = np.array([0, 0, 0], dtype=np.uint8)

        _gamma[i] = a
    return _gamma


gamma = make_gamma()


if __name__ == "__main__":
    args = sys.argv
    do_video = False
    do_cosmics = False
    if 'cosmics' in sys.argv:
        do_cosmics = True
    if 'video' in sys.argv:
        do_video = True
    fpsClock = pygame.time.Clock()
    FPS = 30 # kinect only outputs 30 fps
    disp_size = (640, 480)
    pygame.init()
    screen = pygame.display.set_mode((640, 480))
    font = pygame.font.Font('/usr/share/fonts/truetype/freefont/FreeSerif.ttf', 32) # provide your own font 
    while True:
        events = pygame.event.get()
        for e in events:
            if e.type == pygame.QUIT:
                sys.exit()
        fps_text = "FPS: {0:.2f}".format(fpsClock.get_fps())
        
        # draw the pixels
        # depth array
        depth = np.rot90(get_depth()[0]) # get the depth readings from the camera
        # video array
        if do_video:
            video = np.rot90(get_video()[0])
        pixels = gamma[depth] # the colour pixels are the depth readings overlayed onto the gamma table
##        print pixels
        temp_surface = pygame.Surface(disp_size)

        if do_video:
            im = Image.fromarray(video, 'RGB')
        elif do_cosmics:
            #take pixels (depth colors):
            im = Image.fromarray(pixels, 'RGB')


        # take the video array --> frame --> draw on the frame --> convert to array called pixels
        if do_cosmics:
            cosmic = playkinect.cosmic_muon()
            cosmic.move()
            frame = playkinect.drawtrajectory(im, cosmic.posy, cosmic.posx, cosmic.inity, cosmic.initx, cosmic.color)


        # take video:
        

        if do_cosmics:
            input_value = gpio.input(17)
            if input_value == False:
                print 'you launched a collision!'
                parts = [playkinect.collision_particle() for i in range(20)]
                for part in parts:
                    part.move(depth)
                    frame = playkinect.drawtrajectory(im, part.posy, part.posx, part.inity, part.initx, part.color)
            else:
                print 'no button pressed'
        # depth[2][3]
        # 700
        if do_cosmics:
            if not do_video:
                inverted = ImageOps.invert(im)
                im = inverted
            pixels = np.asarray(im)
        #pixels = gamma(depth)

        pygame.surfarray.blit_array(temp_surface, pixels)
        pygame.transform.scale(temp_surface, (640, 480), screen)
        screen.blit(font.render(fps_text, 1, (255, 255, 255)), (30, 30))
        pygame.display.flip()
        fpsClock.tick(FPS)
