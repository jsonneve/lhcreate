# main.py

from __future__ import division
from flask import Flask, render_template, Response
import numpy as np
#from camera import VideoCamera
import Image
from PIL import Image as pimage, ImageDraw as pimagedraw
import time
from scipy import constants
import math
from skimage.draw import line_aa

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')


image = Image.open('catsleep.jpg').convert('RGBA')
sizex, sizey = image.size
sizex, sizey = (640, 480)
realsizex, realsizey = (5, 5) # 5m by 5m in real life
class cosmic_muon():


    def __init__(self):
        self.velocity = 0.9997* constants.c
        self.angle = self.random_muon_angle()
        self.posx, self.posy = self.initx, self.inity = self.random_muon_init(sizex, sizey)
        self.timestart =  time.time()
        self.color = 'magenta'
        self.color = 'green'

    def random_muon_angle(self):
        """
        """
        return np.random.normal(loc=0, scale =math.pi/4.) + math.pi/2.

    def random_muon_init(self, sizex, sizey):
        """
        """
        return abs(np.random.normal(loc=0, scale =1)) * sizex, 0

    def move(self):
        """
        """
        self.posx = math.cos(self.angle) * (time.time() - self.timestart) * self.velocity / realsizex * sizex + self.initx
        self.posy = math.sin(self.angle) * (time.time() - self.timestart) * self.velocity / realsizey * sizey + self.inity

class particle():


    def __init__(self):
        self.velocity = constants.c
        self.angle = self.random_angle()
        self.posx, self.posy = self.initx, self.inity = self.random_init(sizex, sizey)
        self.timestart =  time.time()
        self.color = 'magenta'

    def random_angle(self):
        """
        """
        return np.random.normal(loc=math.pi/2., scale = math.pi/2.)

    def random_init(self, sizex, sizey):
        """
        """
        return 0, abs(np.random.normal(loc=0, scale =1)) * sizey # from the side

    def move(self):
        """
        """
        self.posx = math.cos(self.angle) * (time.time() - self.timestart) * self.velocity / realsizex * sizex + self.initx
        self.posy = math.sin(self.angle) * (time.time() - self.timestart) * self.velocity / realsizey * sizey + self.inity


class collision_particle():

    def __init__(self, absorbing_box=(100, 100, 800, 300)):
        #particle.__init__(self)
        self.posx, self.posy = self.initx, self.inity = self.random_init(sizex, sizey)
        self.angle = self.random_angle()
        #slow = 1e-10
        slow = 1.
        self.velocity = slow * constants.c
        self.timestart =  time.time()
        self.color = 'cyan'
        self.curve = (0, 359)
        self.penetration = 0.
        #self.jump()
        #self.absorbing_box = [(x, y) for x in range(absorbing_box[0], absorbing_box[1] for y in range(absorbingbox[1], absorbing_box[3])]

    def random_angle(self):
        """
        """
        #return np.random.normal(loc=0, scale =math.pi/2.)
        return np.random.normal(loc=math.pi/2., scale = math.pi/2.)

    def random_init(self, sizex, sizey):
        """
        """
        return 0, sizey/2. # one interaction point
        #return 0, abs(np.random.normal(loc=0, scale =1)) * sizey # from the side

    def move(self, deptharray):
        """
        """
        oldx = self.posx
        oldy = self.posy
        scale = 10.
        #self.posx = math.cos(self.angle) * (time.time() - self.timestart) * self.velocity / realsizex * sizex + self.initx
        #self.posy = math.sin(self.angle) * (time.time() - self.timestart) * self.velocity / realsizey * sizey + self.inity

        did_not_hit_person = True
        while did_not_hit_person:
            self.posx += scale * 1. * math.cos(self.angle)
            self.posy += scale * 1. * math.sin(self.angle)
            self.posyarc = self.posy + 100 if self.angle < math.pi/2. else self.posy -100.
            newposx = int(self.posx - self.penetration * math.cos(self.angle))
            newposy = int(self.posy - self.penetration*math.sin(self.angle))
            if newposx < 0 or newposx >= len(deptharray) or newposy < 0 or newposy >= len(deptharray[0]):
                did_not_hit_person = False
                continue
            if deptharray[newposx][newposy] > 1800:
                print self.posx, self.posy, deptharray[int(self.posx - self.penetration * math.cos(self.angle))][int(self.posy - self.penetration*math.sin(self.angle))]
                self.posx = oldx
                self.posy = oldy
                did_not_hit_person = False
        #minx = self.absorbing_box[0] + self.penetration
        #maxx = self.absorbing_box[2] - self.penetration
        #miny = self.absorbing_box[1] + self.penetration
        #maxy = self.absorbing_box[3] - self.penetration

    def jump(self):
        """
        """
        self.posx = math.cos(self.angle) * (time.time() - self.timestart) * self.velocity / realsizex * sizex + self.initx
        self.posy = math.sin(self.angle) * (time.time() - self.timestart) * self.velocity / realsizey * sizey + self.inity


def dummy_person(image, penetration = 0, absorbing_box=(100, 100, 800, 300)):
    """
    """
    minx = absorbing_box[0] + penetration
    maxx = absorbing_box[2] - penetration
    miny = absorbing_box[1] + penetration
    maxy = absorbing_box[3] - penetration
    person = []
    for pix in [(pixx, pixy) for pixx in range(0, image.size[0]) for pixy in range(0, image.size[1])]:
        if minx < pix[0] < maxx and miny < pix[1] < maxy:
            person.append(pix)
    return person

#def random_muon_init(sizex, sizey):
#        """
#        """
#        return abs(np.random.normal(loc=0, scale =1)) * sizex, 0
#
#def random_muon_angle():
#        """
#        """
#        return np.random.normal(loc=0, scale =math.pi/2.)
#
#def muon_velocity():
#    #slow = 0.000000001
#    slow = 1.0
#    return 0.9997* slow * constants.c
#

#def gen(camera):
def gen():
    timestart = time.time()
    im = Image.open('catsleep.jpg').convert('RGBA')
    sizex, sizey = im.size
    #angle = random_muon_angle()
    #posx, posy = initx, inity = random_muon_init(sizex, sizey)
    #vx = math.cos(angle) * muon_velocity()
    #vy = math.sin(angle) * muon_velocity()
    cosmic = cosmic_muon()
    cosmics = range(10)
    parts = [collision_particle(absorbing_box=(100, 100, 800, 300)) for i in range(20)]
    while True:
        frame = dummyimage() #camera.get_frame()
        im = Image.open('catsleep.jpg').convert('RGBA')
        sizex, sizey = im.size
        realsizex, realsizey = (5, 5) # 5m by 5m in real life
        time_elapsed = int((time.time() - timestart))
        #if time_elapsed % 10. == 0:
        #    cosmics[time_elapsed % 10] = cosmic_muon()

        #for cosm in cosmics:
        #    cosmic.move()
        collisions = False #True
        if collisions: # and time_elapsed % 20 == 0:
            for part in parts:
                part.move(im)
                print part.angle, part.posx, part.posy, part.initx, part.inity
                frame = drawtrajectory(im, part.posx, part.posy, part.initx, part.inity, part.color)
                #frame = drawarc(im, (part.initx, part.inity, part.posx, part.posy), part.curve, color=part.color)

            #if posx > sizex and posy > sizey:
            #    angle = random_muon_angle()
            #    vx = math.cos(angle) * muon_velocity()
            #    vy = math.sin(angle) * muon_velocity()
            #    initx, inity = random_muon_init(sizex, sizey)
            #posx = (time.time() - timestart) * vx / realsizex * sizex + initx
            #posy = (time.time() - timestart) * vy / realsizey * sizey + inity
            time.sleep(1)
        else: # True:
        #    cosmic = cosmic_muon()
        #    cosmic.move()
            cosmic = cosmic_muon()
            cosmic.move()
            frame = drawtrajectory(im, cosmic.posx, cosmic.posy, cosmic.initx, cosmic.inity, cosmic.color)
            #print cosmic.angle, cosmic.posx, cosmic.posy, cosmic.initx, cosmic.inity
        #im = Image.open('catsleep.jpg').convert('RGBA')
        frame = drawrectangle(im, 100, 100, 800, 300)
        #frame = drawtrajectory(im, 0, 0, 300, 400)
        #frame = im.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        time.sleep(.2)


@app.route('/video_feed')
def video_feed():
    return Response(gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')
    #return Response(gen(VideoCamera()),
    #                mimetype='multipart/x-mixed-replace; boundary=frame')

def make_video(images, outimg=None, fps=5, size=None,
               is_color=True, format="XVID"):
    """
    Create a video from a list of images.
 
    @param      outvid      output video
    @param      images      list of images to use in the video
    @param      fps         frame per second
    @param      size        size of each frame
    @param      is_color    color
    @param      format      see http://www.fourcc.org/codecs.php
    @return                 see http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html
 
    The function relies on http://opencv-python-tutroals.readthedocs.org/en/latest/.
    By default, the video will have the size of the first image.
    It will resize every image to this size before adding them to the video.
    """
    from cv2 import VideoWriter, VideoWriter_fourcc, imread, resize
    fourcc = VideoWriter_fourcc(*format)
    vid = None
    for image in images:
        if not os.path.exists(image):
            raise FileNotFoundError(image)
        img = imread(image)
        if vid is None:
            if size is None:
                size = img.shape[1], img.shape[0]
            vid = VideoWriter(outvid, fourcc, float(fps), size, is_color)
        if size[0] != img.shape[1] and size[1] != img.shape[0]:
            img = resize(img, size)
        vid.write(img)
    vid.release()
    return vid

def dummyimage():
    """
    """
    #ret, jpeg = cv2.imencode('.jpg', Image.new('RGB', (100,100)))
    #im = Image.open('catsleep.jpg')
    #return im.tobytes()
    return open('catsleep.jpg', 'r').read()
    #return  Image.new('RGB', (100,100)).tobytes()
    #return Image.new('RGB', (100,100))


def drawtrajectory(im, posx, posy, initx=0, inity=0, color='blue'):
    """
    draw line
    """
    draw = pimagedraw.Draw(im) 
    draw.line((posx, posy, initx, inity), width = 1, fill=color) #228, color=color)
    im.save('catz.jpg')
    frame = open('catz.jpg').read()
    #return open('catsleep.jpg', 'r').read()
    return frame

def draw_line_on_array(ar, posx, poxsy, initx, inity, color='blue'):
    """
    draw a line in an array.
    """
    rr, cc, val = line_aa(initx, inity, posx, posy)
    ar[rr, cc] = val * 255
    return ar

def drawrectangle(im, posx, posy, initx=0, inity=0, color='blue'):
    """
    draw line
    """
    draw = pimagedraw.Draw(im) 
    draw.rectangle((posx, posy, initx, inity)) #228, color=color)
    im.save('catz.jpg')
    frame = open('catz.jpg').read()
    #return open('catsleep.jpg', 'r').read()
    return frame


def drawarc(im, box, angles, color='blue'):
    """
    draw line
    """
    draw = pimagedraw.Draw(im) 
    draw.arc(box, angles[0], angles[1], fill=color) #228, color=color)
    im.save('catz.jpg')
    frame = open('catz.jpg').read()
    #return open('catsleep.jpg', 'r').read()
    return frame



if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
